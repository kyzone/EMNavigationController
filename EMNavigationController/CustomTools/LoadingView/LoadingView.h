//
//  LoadView.h
//  LLYG
//
//  Created by EasonWang on 13-9-24.
//  Copyright (c) 2013年 ShiTengTechnology. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoadingViewDelegate <NSObject>

/**
	loadView 加载失败调用
 */
-(void)loadViewFailure;


@end


@interface LoadingView : UIView
<
    UIAlertViewDelegate
>
{
    LoadingView *_loadView;
    UIActivityIndicatorView* mJuhua;
    NSTimer *loadTimer; // 加载等待的时间，默认为 15s
    
    //NSString *mText;
}

//@property (nonatomic, retain) NSString *mText;

@property(nonatomic,weak)id<LoadingViewDelegate> lDelegate;

/**
	初始化默认的加载动画
	@returns 返回初始化的对象
 */
-(id)initWithDefault;

/**
 *	@brief	初始化默认的加载动画
 *
 *	@return	返回初始化的对象
 */
+ (id)loadingView;


-(id)initWithDefault:(UIViewController*)controller;

/**
	初始化方法
	@returns 返回初始化后的对象
 */
- (id)initWithView:(UIView*) view;



/**
	预备显示加载动画
 */
- (void) willShow;

/**
	预备带参数的加载动画
	@param text 需要显示的内容
 */
- (void) willShowWithMessage:(NSString*)text;

/**
	启动加载动画
 */
-(void)startAnimating;

/**
	关闭加载动画
 */
-(void)stopAnimating;



@end
